resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

data_file 'HANDLING_FILE' 'Palio/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'Palio/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'Palio/carvariations.meta'


files {
  'Palio/handling.meta',
  'Palio/vehicles.meta',
  'Palio/carvariations.meta',

}

client_script 'vehicle_names.lua'
