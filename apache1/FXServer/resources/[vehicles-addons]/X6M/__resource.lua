resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

data_file 'HANDLING_FILE' 'x6m/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'x6m/vehicles.meta'
data_file 'CARCOLS_FILE' 'x6m/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'x6m/carvariations.meta'


files {
  'x6m/handling.meta',
  'x6m/vehicles.meta',
  'x6m/carcols.meta',
  'x6m/carvariations.meta',
  
  
}

client_script 'vehicle_names.lua'
