
local cfg = {}
-- list of weapons for sale
-- for the native name, see https://wiki.fivem.net/wiki/Weapons (not all of them will work, look at client/player_state.lua for the real weapon list)
-- create groups like for the garage config
-- [native_weapon_name] = {display_name,body_price,ammo_price,description}
-- ammo_price can be < 1, total price will be rounded

-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.gunshop_types = {
  ["melees"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_BOTTLE"] = {"Bottle",10000,0,""},
    ["WEAPON_BAT"] = {"Bat",15000,0,""},
    ["WEAPON_KNUCKLE"] = {"Knuckle",15000,0,""},
    ["WEAPON_KNIFE"] = {"Knife",20000,0,""}
  },

  ["sidearms"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_ASSAULTRIFLE"] = {"Assault Rifle",250000,0,""},
    ["WEAPON_PISTOL"] = {"Pistol",65000,0,""},
    ["ARMOR"] = {"Body Armor",20000,0,""}
  },

  ["submachineguns"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_MICROSMG"] = {"Mini SMG",150000,0,""},
    ["WEAPON_SMG"] = {"SMG",200000,0,""},
    ["WEAPON_ASSAULTSMG"] = {"Assault SMG",550000,0,""},
    ["WEAPON_COMBATPDW"] = {"Combat PDW",750000,0,""},
    ["WEAPON_MACHINEPISTOL"] = {"Machine Pistol",75000,0,""},
    ["WEAPON_NIGHTSTICK"] = {"Nighstick",30000,0,""},
    ["WEAPON_CROWBAR"] = {"Crowwbar",30000,0,""},
    ["WEAPON_GOLFCLUB"] = {"Golf club",35000,0,""},
    ["WEAPON_SWITCHBLADE"] = {"Blade",40000,0,""},
    ["WEAPON_MACHETE"] = {"Machete",45000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  },

  ["rifles"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol",15000,0,""},
    ["WEAPON_SNSPISTOL"] = {"Pistol",25000,0,""},
    ["WEAPON_COMPACTRIFLE"] = {"Mini SMG",200000,0,""},
    ["WEAPON_ASSAULTRIFLE"] = {"Assault Rifle",200000,0,""},
    ["WEAPON_CARBINERIFLE"] = {"Carabine Rifle",200000,0,""},
    ["WEAPON_GRENADE"] = {"Grenade",500000,0,""},
    ["WEAPON_MOLOTOV"] = {"Molotv",150000,0,""},
    ["WEAPON_FLARE"] = {"Flare",20000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  },

  ["heavymachineguns"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_GUSENBERG"] = {"Gusenberg MG",200000,0,""},
    ["WEAPON_MG"] = {"MG",250000,0,""},
    ["WEAPON_COMBATMG"] = {"Combat MG",500000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  },

  ["snipers"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol",15000,0,""},
    ["WEAPON_SNSPISTOL"] = {"Pistol",25000,0,""},
    ["WEAPON_MARKSMANRIFLE"] = {"Marksman Rifle",150000,0,""},
    ["WEAPON_SNIPERRIFLE"] = {"Sniper Rifle",200000,0,""},
    ["WEAPON_HEAVYSNIPER"] = {"Heavy Rifle",500000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  },

  ["carabines"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_ASSAULTRIFLE"] = {"Assault Rifle",250000,0,""},
    ["WEAPON_PISTOL"] = {"Pistol",65000,0,""},
    ["ARMOR"] = {"Body Armor",20000,0,""}
  },

  ["shotguns"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_ASSAULTRIFLE"] = {"Assault Rifle",250000,0,""},
    ["WEAPON_PISTOL"] = {"Pistol",65000,0,""},
    ["ARMOR"] = {"Body Armor",20000,0,""}
  },

  ["explosives"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_GRENADELAUNCHER_SMOKE"] = {"Grenade Launcher",1000000,100,""},
    ["WEAPON_FIREEXTINGUISHER"] = {"Fire Extinguisher",1000,0,""},
    ["WEAPON_FIREWORK"] = {"Firework",1000000,0,""},
    ["WEAPON_SNOWBALL"] = {"SnowBall",30,0,""},
    ["WEAPON_FLASHLIGHT"] = {"FlashLight",500,0,""},
    ["WEAPON_STUNGUN"] = {"Stungun",10000,0,""},
    ["WEAPON_MUSKET"] = {"Musket",150000,0,""},
    ["WEAPON_FLAREGUN"] = {"Flaregun",50000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  },

  ["basic"] = {
    _config = {blipid=110,blipcolor=75},
    ["WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol",15000,0,""},
    ["WEAPON_SNSPISTOL"] = {"SNS Pistol",25000,0,""},
    ["WEAPON_GRENADE"] = {"Grenade",500000,0,""},
    ["WEAPON_SMOKEGRENADE"] = {"Smoke Grenade",50000,0,""},
    ["WEAPON_PETROLCAN"] = {"Petrol",5000,0,""},
    ["ARMOR"] = {"Body Armor",10000,0,""}
  }
}

-- list of gunshops positions

cfg.gunshops = {
  {"melees", 1692.41, 3758.22, 34.7053},
  {"carabines", 844.299, -1033.26, 28.1949},
  {"shotguns", 21.70, -1107.41, 29.79},
}

return cfg
