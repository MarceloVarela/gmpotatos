
local cfg = {}

-- paycheck and bill for users
cfg.message_paycheck = "Você recebeu seu salario: ~g~$" -- message that will show before payment of salary
cfg.message_bill = "Pagamento de contas: ~r~$" -- message that will show before payment of bill
cfg.post = "." -- message that will show after payment

cfg.bank = true -- if true money goes to bank, false goes to wallet

cfg.minutes_paycheck = 30 -- minutes between payment for paycheck
cfg.minutes_bill = 30 -- minutes between withdrawal for bill

cfg.paycheck_title_picture = "Banco do Brasil" -- define title for paycheck notification picture
cfg.paycheck_picture = "CHAR_BANK_MAZE" -- define paycheck notification picture want's to display
cfg.bill_title_picture = "Compania de Seguro" -- define title for bill notification picture
cfg.bill_picture = "CHAR_MP_MORS_MUTUAL" -- define bill notification picture want's to display

cfg.paycheck = { -- ["permission"] = paycheck
  ["police.paycheck"] = 2300,
  ["citizen.paycheck"] = 1000,
--[""] = 0,
  --SAMU
  ["paramedico.paycheck"] = 8000,
  ["enfermeiro.paycheck"] = 10000,
  ["medico.paycheck"] = 10000,
  ["diretor.paycheck"] = 10000,
  -- SAMU
  -- Policia
  ["cm.paycheck"] = 12600,
  ["coronel.paycheck"] = 12100,
  ["tenencoronel.paycheck"] = 11600,
  ["major.paycheck"] = 11100,
  ["capitao.paycheck"] = 10600, 
  ["tenente2.paycheck"] = 10100,
  ["tenente1.paycheck"] = 9600,
  ["sargento3.paycheck"] = 7600,
  ["sargento2.paycheck"] = 7100,
  ["sargento1.paycheck"] = 6600,
  ["cabo.paycheck"] = 6100,
  ["soldado.paycheck"] = 5600,
  ["recruta.paycheck"] = 5100,  
  -- Fim Policia
  -- PF
  ["delegado.paycheck"] = 12100,
  ["oficial.paycheck"] = 11600,
  ["agente.paycheck"] = 11100,
  -- PF FIM
  -- Vip
  ["vip2.paycheck"] = 4000,
  ["vip3.paycheck"] = 8000,
  -- Vip

  ["taxi.paycheck"] = 800,
  ["repair.paycheck"] = 800,
  ["bankdriver.paycheck"] = 1200,
  ["delivery.paycheck"] = 500,
  ["Lawyer.paycheck"] = 5100
}

cfg.bill = { -- ["permission"] = withdrawal
  ["police.paycheck"] = 450,
--[""] = 0,
  ["emergency.paycheck"] = 150,
  ["taxi2.paycheck"] = 125,
  ["repair2.paycheck"] = 100,
  ["bankdriver2.paycheck"] = 220,
  ["delivery2.paycheck"] = 350,
  ["Lawyer2.paycheck"] = 250
}

return cfg

